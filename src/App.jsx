import { useState } from "react";
import "./App.css";

function App() {
  const [color, setColor] = useState("#000");

  return (
    <div
      className="w-full h-screen duration-200"
      style={{ backgroundColor: color }}
    >
      <div className="fixed bottom-12 flex flex-wrap justify-center px-2 inset-x-0">
        <div className="flex flex-wrap gap-3 justify-center bg-slate-50 px-3 py-2 rounded-xl shadow-2xl border border-black">
          <button
            className="outline-none border-none px-3 py-2 rounded-xl text-white shadow-2xl"
            style={{ backgroundColor: "red" }}
            // onClick={setColor("red")}
            // We can't do that because onClick() method want a Function and when we pass the reference [setColor], so we can't give color name as a parameter tp function which changes the bg color of the body and if we give the parameter also i.e. [setColor('red')], so it executes that function immediately and we don't want that. We want only when use user click on that button then bg color of body gets changed. So we pass a callback because onClick( () => setColor("green")) method also accept a callback.
            onClick={() => setColor("red")}
          >
            RED
          </button>
          <button
            className="outline-none border-none px-3 py-2 rounded-xl text-white shadow-2xl"
            style={{ backgroundColor: "green" }}
            onClick={() => setColor("green")}
          >
            GREEN
          </button>
          <button
            className="outline-none border-none px-3 py-2 rounded-xl text-white shadow-2xl"
            style={{ backgroundColor: "blue" }}
            onClick={() => setColor("blue")}
          >
            BLUE
          </button>
          <button
            className="outline-none border-none px-3 py-2 rounded-xl text-black shadow-2xl"
            style={{ backgroundColor: "pink" }}
            onClick={() => setColor("pink")}
          >
            PINK
          </button>
          <button
            className="outline-none border-none px-3 py-2 rounded-xl text-white shadow-2xl"
            style={{ backgroundColor: "olive" }}
            onClick={() => setColor("olive")}
          >
            OLIVE
          </button>
          <button
            className="outline-none border-none px-3 py-2 rounded-xl text-black shadow-2xl"
            style={{ backgroundColor: "lavender" }}
            onClick={() => setColor("lavender")}
          >
            LAVENDER
          </button>
          <button
            className="outline-none border-none px-3 py-2 rounded-xl text-black shadow-2xl"
            style={{ backgroundColor: "aqua"}}
            onClick={() => setColor("aqua")}
          >
            AQUA
          </button>
          <button
            className="outline-none border-none px-3 py-2 rounded-xl text-black shadow-2xl"
            style={{ backgroundColor: "silver" }}
            onClick={() => setColor("silver")}
          >
            SILVER
          </button>
          <button
            className="outline-none border-none px-3 py-2 rounded-xl text-black shadow-2xl"
            style={{ backgroundColor: "yellow" }}
            onClick={() => setColor("yellow")}
          >
            YELLOW
          </button>
          <button
            className="outline-none border-none px-3 py-2 rounded-xl text-black shadow-2xl"
            style={{ backgroundColor: "violet" }}
            onClick={() => setColor("violet")}
          >
            VOILET
          </button>
        </div>
      </div>
    </div>
  );
}

export default App;
